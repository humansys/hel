  var express = require('express')
  var app = express() 
  
  app.get('/', function (req, res) {
    res.send('Bienvenidos al Webinar de Atlassian Open DevOps este jueves 11 de junio!')
  })

  app.listen(3000, function () {
      console.log('Example app listening on port 3000!')
   }) 

  module.exports = app;
